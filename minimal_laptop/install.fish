#! /bin/fish

#install config files and backup old config files
set INSTALL_FOLDERS (ls config/)

#backup names by date
set BACKUP_FOLDER $HOME/.backup/(date)/
mkdir -p $BACKUP_FOLDER

#numbering backups
#set BACKUP_FOLDER "../backup/"(count (ls ../backup))"/"

for folder in $INSTALL_FOLDERS;
    mv  $HOME/.config/$folder $BACKUP_FOLDER;
    cp -r ./config/$folder $HOME/.config;
end
chmod +rx -R ./config/

#set wallpaper
nitrogen --set-auto --save ./wallpaper.*

#reload
bspc vm -r
