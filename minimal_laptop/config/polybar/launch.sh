#!/bin/fish

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
#while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

polybar -r --config=~/.config/polybar/config main &

sleep 1

bspc config top_padding 0

echo "Polybar launched..."
