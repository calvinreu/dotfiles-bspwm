# dotfiles bspwm

my bspwm dotfiles and install scripts

Licensed under the MIT License

## requirements

- fish
- yay(or other aur helper)
- arch based system NOTE: themes could contain package lists for ohter repositorys and package managers
- root privilege

## first time setup

### fish
> sudo pacman -Syy && sudo pacman -S fish && sudo pacman -Syu

### yay
 > sudo pacman -S --needed git base-devel &&
 > git clone https://aur.archlinux.org/yay.git &&
 > cd yay &&
 > makepkg -si 

or see install instructions on https://github.com/Jguer/yay

### sxhkd
if you use these files on bspwm copy one file from *path_to_repo*/sxhkd/ to ~/.config/sxhkd/sxhkdrc or use a custom file
 > cd dotfiles-bspwm

 > mkdir ~/.config/sxhkd && cp sxhkd/sxhkdrc-*your_version* ~/.config/sxhkd/sxhkdrc

## install theme

### open terminal in theme directory

 > cd *path_to_theme*

### install dependencies

#### on fish
 > sudo pacman -S --needed (cat dependencies-pacman | cut -d ' ' -f1)

 > yay -S --needed (cat dependencies-pacman | cut -d ' ' -f1)

#### on bash
 > sudo pacman -S --needed $(cat dependencies-pacman | cut -d ' ' -f1)

 > yay -S --needed $(cat dependencies-pacman | cut -d ' ' -f1)

### run install script
 > ./install.fish

## backups
theme installations move files/folders with the same names as their own config files to a backup folder in ~/.backup/*date_and_time*/ if the installation breaks something open a terminal in the backup of the previous config and run the recover.fish file
NOTE: this could not work if the configuration files have been modified after installing a theme
 > cd ~/.backup/*name_of_backup*

 > *path_to_git_repo*/recover.fish

this will also create a backup of the configuration so it can be modified to work for you

## creating themes
to create a theme copy the theme_defaults directory if necessary modify the files in you copied.
 > cp -r *path_to_git_repo*/theme_defaults


rename the folder to whatever you want.

then cd into it
 > cd *theme name*

choose a wallpaper and copy it into your themes directory and name it wallpaper.*img_extension*

edit the dependency files to include the packages required to use your config.

create a folder called config and put every file which should be copied into ~/.config into it.

check if the install.fish will install your theme properly if not modify it.

NOTE: if the install.fish won`t be undone by the recover.fish create a new recover.fish place it into your theme folder and create a notice how to recover a backup after installing your theme.

it would be appreciated if you would add your theme to this repository or add a link to a repository with your theme in the README